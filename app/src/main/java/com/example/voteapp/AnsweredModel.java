package com.example.voteapp;

public class AnsweredModel {
    private Integer id;
    private String answeredQuestion;
    private String answeredAnswer;

    public AnsweredModel(Integer id, String answeredQuestion, String answeredAnswer) {
        this.id = id;
        this.answeredQuestion = answeredQuestion;
        this.answeredAnswer = answeredAnswer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnsweredQuestion() {
        return answeredQuestion;
    }

    public void setAnsweredQuestion(String answeredQuestion) {
        this.answeredQuestion = answeredQuestion;
    }

    public String getAnsweredAnswer() {
        return answeredAnswer;
    }

    public void setAnsweredAnswer(String answeredAnswer) {
        this.answeredAnswer = answeredAnswer;
    }
}
