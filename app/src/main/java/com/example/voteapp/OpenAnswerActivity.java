package com.example.voteapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OpenAnswerActivity extends AppCompatActivity {
    private static dbConnectionPG con = new dbConnectionPG();

    private RecyclerView recyclerViewAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_answer);


        List<Answer> answers = getListAnswers();
        this.recyclerViewAnswer = (RecyclerView)this.findViewById(R.id.recyclerViewOpenAnswers);
        recyclerViewAnswer.setAdapter(new OpenAnswerAdapter(this, answers));

        LinearLayoutManager linearLayoutManagerA = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewAnswer.setLayoutManager(linearLayoutManagerA);
    }

    private List<Answer> getListAnswers(){
        List<Answer> list = new ArrayList<Answer>();

        Integer userID = getIntent().getIntExtra("userid", 0);
        Integer questionid = getIntent().getIntExtra("questionID", 104);
        System.out.println("Question ID: "+questionid);

        String sql ="select distinct answer.id, answer.answer\n" +
                "from votedb\n" +
                "join answer on votedb.answer_id = answer.id\n" +
                "join vote on votedb.vote_id = vote.id\n" +
                "where vote.questions = (\n" +
                "select distinct questions\n" +
                "from vote\n" +
                "where id = '"+questionid.toString()+"')";

        try {
            Statement statement = con.connectionDB().createStatement();

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                Integer id = rs.getInt("id");
                String answer = rs.getString("answer");

                list.add(new Answer(id, answer));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return  list;
    }
}