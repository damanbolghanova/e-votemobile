package com.example.voteapp;

import java.util.LinkedList;
import java.util.List;

public class Question {
    private static Question instances;

    public String question;
    public Integer id;

    public Question() {
    }

    public String getQuestion(){
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Question(String question, Integer id) {
        this.question = question;
        this.id = id;
    }

    public static Question getInstance() {
        if (instances == null){
            instances = new Question();
        }
        return instances;
    }
}
