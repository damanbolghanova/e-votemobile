package com.example.voteapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AnsweredAdapter extends RecyclerView.Adapter<AnsweredViewHolder>{
    private List<AnsweredModel> answered;
    private Context context;
    private LayoutInflater mLayoutInflater;

    public AnsweredAdapter(Context context, List<AnsweredModel> answered) {
        this.context = context;
        this.answered = answered;
        this.mLayoutInflater = LayoutInflater.from(context);
    }


    @Override
    public AnsweredViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View recyclerViewItem = mLayoutInflater.inflate(R.layout.recyclerview_answered_layout, parent,false);
        recyclerViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleRecyclerItemClick((RecyclerView) parent, v);
            }
        });
        return new AnsweredViewHolder(recyclerViewItem);
    }

    @Override
    public void onBindViewHolder(AnsweredViewHolder holder, int position) {
        AnsweredModel model = this.answered.get(position);

        holder.questionIDanswered.setText(model.getAnsweredQuestion());
        holder.answerIDanswered.setText(model.getAnsweredAnswer());

    }

    @Override
    public int getItemCount() {
        return this.answered.size();
    }

    private void handleRecyclerItemClick(RecyclerView recyclerView, View itemView) {
        int itemPosition = recyclerView.getChildLayoutPosition(itemView);
        AnsweredModel model = this.answered.get(itemPosition);

        Toast.makeText(this.context, model.getAnsweredAnswer(), Toast.LENGTH_LONG).show();
    }
}
