package com.example.voteapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.ls.LSOutput;

import java.util.List;

public class OpenQuestionAdapter extends RecyclerView.Adapter<OpenQuestionAdapter.QuestionsViewHolder> {
    private Context mcontext;
    private List<Question> questionList;
    private LayoutInflater mLayoutInflater;

    public OpenQuestionAdapter(Context context, List<Question> questionList) {
        this.mcontext = context;
        this.questionList = questionList;
        this.mLayoutInflater = mLayoutInflater.from(context);
    }

    @Override
    public QuestionsViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View recyclerViewItem = mLayoutInflater.inflate(R.layout.recyclerview_openquestions_layout, parent,false);
        recyclerViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleRecyclerItemClick((RecyclerView) parent, v);
            }
        });
        return new OpenQuestionAdapter.QuestionsViewHolder(recyclerViewItem);
    }

    @Override
    public void onBindViewHolder(QuestionsViewHolder holder, int position) {
        Question question= this.questionList.get(position);

        holder.questionBtn.setText(question.getQuestion());


    }

    @Override
    public int getItemCount() {
        return this.questionList.size();
    }

    private void handleRecyclerItemClick(RecyclerView recyclerView, View itemView) {
        int itemPosition = recyclerView.getChildLayoutPosition(itemView);
        Question question = this.questionList.get(itemPosition);

        Toast.makeText(this.mcontext, question.getQuestion(), Toast.LENGTH_LONG).show();
    }


    public class QuestionsViewHolder extends RecyclerView.ViewHolder {

        Button questionBtn;


        public QuestionsViewHolder(final View itemView) {
            super(itemView);


            this.questionBtn =(Button) itemView.findViewById(R.id.questionText);

            this.questionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context holderContext = itemView.getContext();
                    Intent intent = new Intent(holderContext, OpenAnswerActivity.class);
                    intent.putExtra("questionID", 104);
                    holderContext.startActivity(intent);
                }
            });
        }
    }
}
