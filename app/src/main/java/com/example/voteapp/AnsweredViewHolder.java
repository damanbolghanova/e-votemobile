package com.example.voteapp;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AnsweredViewHolder extends RecyclerView.ViewHolder {

    TextView questionIDanswered, answerIDanswered;

    public AnsweredViewHolder(@NonNull View itemView) {
        super(itemView);

        this.questionIDanswered = (TextView)itemView.findViewById(R.id.questionIDanswered);
        this.answerIDanswered = (TextView) itemView.findViewById(R.id.answerIDanswered);

    }
}
