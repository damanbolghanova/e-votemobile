package com.example.voteapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    GlobalVariables va = GlobalVariables.getInstance();

    TextView fname, lname, name;
    Button btnVotePage, btnOpenQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        name = findViewById(R.id.tv_name);
        btnVotePage = findViewById(R.id.btnVotePage);
        btnOpenQuestions = findViewById(R.id.btnOpenQuestions);

        if (va.get_codeVar()!=0){
            fname.setText(va.get_fnameVar().toString());
            lname.setText(va.get_lnameVar().toString());
            name.setText(va.get_fnameVar()+" " + va.get_lnameVar());
        }

        btnVotePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), VotingActivity.class);
                intent.putExtra("userid", va.get_codeVar());
                startActivity(intent);
            }
        });

        btnOpenQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), OpenQuestionsActivity.class);
                intent.putExtra("userid", va.get_codeVar());
                startActivity(intent);
            }
        });
    }
}