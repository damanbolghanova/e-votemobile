package com.example.voteapp;

import android.content.Intent;

public class Answer {
    private static Answer instances;

    private Integer id;
    private String answerTxt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnswerTxt() {
        return answerTxt;
    }

    public void setAnswerTxt(String answerTxt) {
        this.answerTxt = answerTxt;
    }

    public Answer(Integer id, String answerTxt) {
        this.id = id;
        this.answerTxt = answerTxt;
    }

    public Answer() {
    }

    public static synchronized Answer getInstance(){
        if (instances == null){
            instances = new Answer();
        }
        return instances;
    }
}
