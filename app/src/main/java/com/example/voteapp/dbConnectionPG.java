package com.example.voteapp;

import java.sql.Connection;
import java.sql.DriverManager;

public class dbConnectionPG {
    Connection connection = null;

    public Connection connectionDB(){
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://10.0.2.2:5432/sv", "postgres", "welldone");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return connection;
    }

    protected void close_connection(Connection con) throws Exception{
        con.close();
    }

}
