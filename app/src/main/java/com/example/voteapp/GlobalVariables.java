package com.example.voteapp;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

public class GlobalVariables {
    private static GlobalVariables instances;

    private int _codeVar =0;
    private  static String _fnameVar="";
    private  static String _lnameVar="";

    public int get_codeVar() {
        return _codeVar;
    }

    public void set_codeVar(int _codeVar) {
        this._codeVar = _codeVar;
    }

    public  String get_fnameVar() {
        return _fnameVar;
    }

    public  void set_fnameVar(String _fnameVar) {
        GlobalVariables._fnameVar = _fnameVar;
    }

    public  String get_lnameVar() {
        return _lnameVar;
    }

    public  void set_lnameVar(String _lnameVar) {
        GlobalVariables._lnameVar = _lnameVar;
    }


    public static synchronized GlobalVariables getInstance(){
        if (instances == null){
            instances = new GlobalVariables();
        }
        return instances;
    }
}
