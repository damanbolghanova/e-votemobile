package com.example.voteapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class VotingActivity extends AppCompatActivity {

    private static dbConnectionPG con = new dbConnectionPG();

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting);

        List<AnsweredModel> models = getListData();
        this.recyclerView = (RecyclerView)this.findViewById(R.id.recyclerViewVoting);
        recyclerView.setAdapter(new AnsweredAdapter(this, models));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private List<AnsweredModel> getListData(){
        List<AnsweredModel> list = new ArrayList<AnsweredModel>();

        Integer userid = getIntent().getIntExtra("userid", 0);
        System.out.println("User ID: "+userid);

        String sql = "select votedb.id, user_id, questions, answer " +
                "from votedb\n" +
                "join vote on votedb.vote_id=vote.id " +
                "join answer\n" +
                "on votedb.answer_id=answer.id " +
                "where user_id = '"+userid.toString()+"'";

        try {
            Statement statement = con.connectionDB().createStatement();

            ResultSet rs = statement.executeQuery(sql);

                while (rs.next()) {
                    Integer id = rs.getInt("id");
                    String question = rs.getString("questions");
                    String answer = rs.getString("answer");

                    list.add(new AnsweredModel(id, question, answer));

                    System.out.println(list.toString());
                }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return  list;
    }
}