package com.example.voteapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class OpenAnswerAdapter extends RecyclerView.Adapter<OpenAnswerAdapter.AnswersViewHolder> {
    private static dbConnectionPG con = new dbConnectionPG();
    private Context context;
    private List<Answer> answerList;
    private LayoutInflater mLayoutInflater;

    public OpenAnswerAdapter(Context context, List<Answer> answerList) {
        this.context = context;
        this.answerList = answerList;
        this.mLayoutInflater = mLayoutInflater.from(context);
    }

    @Override
    public OpenAnswerAdapter.AnswersViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View recyclerViewItem = mLayoutInflater.inflate(R.layout.recyclerview_answers_layout, parent,false);
        recyclerViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleRecyclerItemClick((RecyclerView) parent, v);
            }
        });
        return new OpenAnswerAdapter.AnswersViewHolder(recyclerViewItem);
    }

    @Override
    public void onBindViewHolder(AnswersViewHolder holder, int position) {
        Answer answer= this.answerList.get(position);

        holder.radioButton.setText(answer.getAnswerTxt());
    }

    @Override
    public int getItemCount() {
        return this.answerList.size();
    }


    private void handleRecyclerItemClick(RecyclerView recyclerView, View itemView) {
        int itemPosition = recyclerView.getChildLayoutPosition(itemView);
        Answer answer = this.answerList.get(itemPosition);

        Toast.makeText(this.context, answer.getAnswerTxt(), Toast.LENGTH_LONG).show();
    }


    public class AnswersViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;

        public AnswersViewHolder(@NonNull View itemView) {
            super(itemView);

            this.radioButton =(RadioButton) itemView.findViewById(R.id.radioButton);

            this.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String sql ="insert into votedb\n" +
                            "values(nextval('votedb_sequence'), 106, 103, 104);";

                    try {
                        Statement statement = con.connectionDB().createStatement();

                        statement.executeUpdate(sql);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                    Intent intent = new Intent(v.getContext(), OpenQuestionsActivity.class);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}
