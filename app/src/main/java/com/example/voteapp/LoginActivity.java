package com.example.voteapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Types;

public class LoginActivity extends AppCompatActivity {

    static final String QUERY = "{call get_users (?,?,?,?,?,?)}";

    private static dbConnectionPG con = new dbConnectionPG();
    GlobalVariables va = GlobalVariables.getInstance();

    Button btnLogin;
    ImageView iconIV;
    EditText txtUsername, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        btnLogin = findViewById(R.id.btnlogin);
        txtPassword = findViewById(R.id.txtpassword);
        txtUsername = findViewById(R.id.txtusername);
        iconIV = findViewById(R.id.iconE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                start_session(txtUsername.getText().toString(), txtPassword.getText().toString());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void start_session(String username, String password){
        try {
            CallableStatement cStmt = con.connectionDB().prepareCall(QUERY);
            cStmt.setString(1, username);
            cStmt.setString(2, password);
            cStmt.registerOutParameter(3, Types.BIGINT);
            cStmt.registerOutParameter(4, Types.VARCHAR);
            cStmt.registerOutParameter(5, Types.VARCHAR);
            cStmt.registerOutParameter(6, Types.VARCHAR);

            cStmt.executeUpdate();
            System.out.println(cStmt.toString());

            Integer _code = Math.toIntExact(cStmt.getLong(3));
            String _fname = cStmt.getString(4);
            String _lname = cStmt.getString(5);
            String _msg = cStmt.getString(6);


            if (_msg.equals("OK")){
                va.set_codeVar(_code);
                va.set_fnameVar(_fname);
                va.set_lnameVar(_lname);
                //System.out.println(_code);
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            }
            else if(_msg.equals("Wrong Credentials!")){
                Toast.makeText(getApplicationContext(), _msg, Toast.LENGTH_SHORT).show();

            }
        }
        catch (Exception e){
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}