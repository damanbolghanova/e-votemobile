package com.example.voteapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OpenQuestionsActivity extends AppCompatActivity {

    private static dbConnectionPG con = new dbConnectionPG();

    private RecyclerView recyclerViewQuestion;

    TextView goToProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_questions);

        List<Question> questions = getListQuestion();
        this.recyclerViewQuestion = (RecyclerView) this.findViewById(R.id.recyclerViewOpenQuestions);
        recyclerViewQuestion.setAdapter(new OpenQuestionAdapter(this, questions));

        LinearLayoutManager linearLayoutManagerQ = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewQuestion.setLayoutManager(linearLayoutManagerQ);


        goToProfile = (TextView) findViewById(R.id.btnGoToProfile);

        goToProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context holderContext = v.getContext();
                Intent intent = new Intent(holderContext, ProfileActivity.class);
                holderContext.startActivity(intent);
            }
        });


    }

    private List<Question> getListQuestion() {
        List<Question> list = new ArrayList<Question>();

        Integer userid = getIntent().getIntExtra("userid", 0);
        System.out.println("User ID: " + userid);

        String sql = "select exists( select distinct vote.questions, vote.id\n" +
                "from votedb\n" +
                "join vote on votedb.vote_id = vote.id\n" +
                "join users on votedb.user_id = users.id\n" +
                "where users.id = '" + userid.toString() + "')";

        try {
            Statement statement = con.connectionDB().createStatement();

            ResultSet rs = statement.executeQuery(sql);

            if (rs.next()) {
                System.out.println("sql2: ");
                        String sql2 = "select questions, id from vote\n"+
                                        "where id != (select vote.id from vote\n"+
                                        "join votedb on votedb.vote_id = vote.id\n"+
                                        "join users on users.id = votedb.user_id\n"+
                                        "where users.id = '" + userid.toString() + "')";

                        try {
                            Statement statement2 = con.connectionDB().createStatement();

                            ResultSet rs2 = statement2.executeQuery(sql2);

                            while (rs2.next()) {
                                Integer id = rs2.getInt("id");
                                String question = rs2.getString("questions");

                                list.add(new Question(question, id));

                            }
                        } catch (SQLException e) {
                            System.out.println(e.getMessage());
                        }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());;
        }
        return list;
    }
}